# Trim.SP - TRIM simulation #

### Contents ###

This repository contains the [Fortran] source code and an accompanying
graphical user interface ([GUI]) for TrimSP -
an application for performing [Monte Carlo] simulations of ion implantation. 

* This code is specifically tuned to simulate implantation of low-energy projectiles in materials.
* The code is maintained by the Low Energy Muons ([LEM]) group at the Paul Scherrer Institute ([PSI]). 
* The [GUI] is written in [Node.js], [JavaScript] and [Electron]. 
* The binary (statically linked) from the [Fortran] code is also included.
* [RPM] and [DEB] packages are also included.

Further information can be found in the following publications:

- J. P. Biersack and W. Eckstein,
  <i>Sputtering studies with the Monte Carlo program TRIM.SP</i>,
  Appl. Phys. A <b>34</b> (2), 73-94 (1984).
  <https://doi.org/10.1007/BF00614759>
- W. Eckstein,
  <i>Computer Simulation of Ion-Solid Interactions</i>,
  Springer Series in Materials Science Vol. 10
  (Springer-Verlag, Berlin, Heidelberg, 1991).
  <https://doi.org/10.1007/978-3-642-73513-4>
- W. Eckstein,
  <i>Backscattering and sputtering with the Monte-Carlo program TRIM.SP</i>,
  Radiat. Eff. Defects Solids <b>130-131</b> (1), 239-250 (1994).
  <https://doi.org/10.1080/10420159408219787>
- E. Morenzoni et al.,
  <i>Implantation studies of keV positive muons in thin metallic layers</i>,
  Nucl. Instrum. Methods Phys. Res., Sect. B <b>192</b> (3), 245-266 (2002).
  <https://doi.org/10.1016/S0168-583X(01)01166-1>

### Supported platforms ###

* [Linux]
* Online at: http://musruser.psi.ch/TrimSP/

### Installation ###

For a simple installation you may use the included RPM or Debian packages which come with statically linked binaries. See instructions below if you prefer a manual installation. 

###### Fortran code compilation ######

Install the `gfortran` compiler, then:

```bash
cd trimsp/fortran
make
make install
```
    
This will install the `trimspNL` binary in `/usr/local/bin`,
but it can be moved to any other directory in your `PATH`.

###### Running the GUI ######

Install [Node.js] (`nodejs`, `nodejs-libs` and `npm`),
then run:

```bash
cd trimsp
npm install
npm start
```

Note: you do not need to run `npm install` every time.
Next time you can simply run `npm start` only. 

### Contact ###

Zaher Salman <zaher.salman@psi.ch>


[Fortran]: https://fortran-lang.org/
[Node.js]: https://nodejs.org/en/
[JavaScript]: https://www.javascript.com/
[Electron]: https://www.electronjs.org/
[PSI]: https://www.psi.ch/en
[LEM]: https://www.psi.ch/en/low-energy-muons
[RPM]: https://rpm.org/
[DEB]: https://en.wikipedia.org/wiki/Deb_(file_format)
[Linux]: https://en.wikipedia.org/wiki/Linux
[Monte Carlo]: https://en.wikipedia.org/wiki/Monte_Carlo_method
[GUI]: https://en.wikipedia.org/wiki/Graphical_user_interface
