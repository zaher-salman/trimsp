// This file contains function that are web app specific

function writeAsciiFile(filename,content) {
    // Write string content into ascii file filename
    // Go via CGI script
    //console.log(filename, content);
    // Prepare CGI args use POST for long files
    let cgiargs = "fn="+filename; // POST
    let lines = content.split(/\n/);
    let prefix = filename.split(/\//);
    for (let i=0; i<lines.length; i++) {
	cgiargs += "&line" + i + "=" + lines[i].replace(/\s\s+/g, ' ');
    }

    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
	    console.log("response=",xhttp.responseText);
	} else if (this.readyState == 4) {
	    console.log(xhttp.status, xhttp.statusText);
	}
    }
    let request = "/cgi-bin/singleTrimSP.cgi"; //POST
    xhttp.open("POST", request, false);   //POST

    //Send the proper header information along with the request
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded'); //POST
    xhttp.send(cgiargs); //POST
    // Now you can add a link to the files
    let d = document.getElementById("linkDiv");
    if (!d) {
	d = document.createElement("div");
	d.id = "linkDiv";
	document.body.appendChild(d);
    }
    let htmlLink = "Download files: <a href='";
    htmlLink += "/tmp/" + prefix[2] + ".tgz'>";
    htmlLink += prefix[2] + ".tgz</a>";
    d.innerHTML = htmlLink;
	
    return(1);
}

function writeAsciiFile2(filename,content) {
    // Write string content into ascii file filename
    // Go via CGI script
    //console.log(filename, content);
    // Prepare CGI args use GET for short files
    let cgiargs = "?fn="+filename; // GET
    let lines = content.split(/\n/);
    let prefix = filename.split(/\//);
    for (let i=0; i<lines.length; i++) {
	cgiargs += "&line" + i + "=" + lines[i].replace(/\s\s+/g, ' ');
    }

    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
	    console.log("response=",xhttp.responseText);
	} else if (this.readyState == 4) {
	    console.log(xhttp.status, xhttp.statusText);
	}
    }
    let request = "/cgi-bin/singleTrimSP.cgi"+cgiargs; // GET
    xhttp.open("GET", request, false);   //GET

    //Send the proper header information along with the request
    xhttp.send(); // GET
    // Now you can add a link to the files
    let d = document.getElementById("linkDiv");
    if (!d) {
	d = document.createElement("div");
	d.id = "linkDiv";
	document.body.appendChild(d);
    }
    let htmlLink = "Download files: <a href='";
    htmlLink += "/tmp/" + prefix[2] + ".tgz'>";
    htmlLink += prefix[2] + ".tgz</a>";
    d.innerHTML = htmlLink;
	
    return(1);
}

function readAsciiFile(filename) {
    // Read ascii file filename and return content in a string
    var xhttp;
    let file = filename;
    if (filename == "" || filename == undefined || filename == null) {
        return 0;
    } else {
        // Make an HTTP request
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
		if (this.status == 404) {
                    console.log("File "+file+" not found.");
		    return(0);
                }
	    }
	}
	// Force refresh, do not use cache
	filename += "?" + Date();
	//filename = "http://musruser.psi.ch" + filename;
        xhttp.open("GET", filename, false);
	xhttp.overrideMimeType("text/plain");
        xhttp.send();
	return(xhttp.responseText);
    }
}
	       
function fileExists(filename) {
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', filename, false);
    xhr.send();
     
    if (xhr.status == "404") {
        return 0;
    } else {
        return 1;
    }
}

