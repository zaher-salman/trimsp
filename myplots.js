function Plot_xy(plotdiv,x,y,labels){
    // Plot (x,y) scatter with x/ylabels
    // place generated plot in div named plotdiv
    //var plotDiv = document.getElementById(plotdiv);

    let xlable = '';
    let ylable = '';
    let legend = '';
    
    
    // labels are arranged as xlable, ylabel, legend
    if (labels[0]) xlabel=labels[0];
    if (labels[1]) ylabel=labels[1];
    if (labels[2]) legend=labels[2];
    
    let traces = [{
	x: x,
	y: y,
	mode: 'lines+markers',
	line: {shape: 'spline'},
	name: legend
    }];
    
    let layout = {
	autosize: true,
	xaxis: {title: xlabel},
	yaxis: {title: ylabel}
    }
    
    Plotly.plot(plotdiv, traces, layout);
    // Resize
    if (plotdiv == "plotRge") resizePl(1);
    if (plotdiv == "plotFrac") resizePl(2);
};

function Plot_xyerr(plotdiv,x, y,dy,xlabel,ylabel){
    // Plot (x,y) scatter with x/ylabels
    // place generated plot in div named plotdiv
    // and y error bars in dy
    var traces = [{
	x: x,
	y: y,
	mode: 'lines+markers',
	error_y: {
	    type: 'data',
	    array : dy,
	    visible: true
	},
    }];
    
    var layout = {
	xaxis: {title: xlabel},
	yaxis: {title: ylabel}
    }
    
    Plotly.plot(plotdiv, traces, layout);
};

function Plot_xyerrpm(plotdiv,x, y,dyp,dym,xlabel,ylabel){
    // Plot (x,y) scatter with x/ylabels
    // place generated plot in div named plotdiv
    // and aymmetric y error bars in dyp and dym
    var plotDiv = document.getElementById("plot");
    var traces = [{
	x: x,
	y: y,
	mode: 'lines+markers',
	error_y: {
	    type: 'data',
	    symmetric: false,
	    array : dyp,
	    arrayminus: dym,
	    visible: true
	},
    }];
    
    var layout = {
	xaxis: {title: xlabel},
	yaxis: {title: ylabel}
    }
    
    Plotly.plot(plotdiv,traces,layout);
};

function sizePlot(){
    var plotDiv=this.document.getElementById("newPlot");
    let newx=this.innerWidth-25;
    let newy=this.innerHeight-25;
    var update ={
	width: newx,
	height: newy
    };
    Plotly.relayout(plotDiv,update);
}

function resizePl(flag){
    let newx=this.innerWidth/2-50;
    let newy=this.innerHeight-185;
    var plotDiv1=this.document.getElementById("plotRge");
    var plotDiv2=this.document.getElementById("plotFrac");
    var update ={
	width: newx,
	height: newy
    };
    if (flag == null) {
	Plotly.relayout(plotDiv1,update);
	Plotly.relayout(plotDiv2,update);
    } else if (flag == 1) {
	Plotly.relayout(plotDiv1,update);
    } else if (	flag == 2) {
	Plotly.relayout(plotDiv2,update);
    }
}
